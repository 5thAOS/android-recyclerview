package com.example.rany.recyclerviewdemo.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rany.recyclerviewdemo.R;
import com.example.rany.recyclerviewdemo.listener.ItemViewListener;
import com.example.rany.recyclerviewdemo.model.Article;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    List<Article> articles;
    ItemViewListener listener;

//    public MyAdapter(List<Article> articles) {
//        this.articles = articles;
//    }

    public void setItemClickListener(ItemViewListener listener){
        this.listener = listener;
    }

    public MyAdapter(){
        articles = new ArrayList<>();
    }

    public void addArticles(List<Article> articleList){
        articles.addAll(articleList);
        notifyDataSetChanged();
    }

    public Object getArticleItem(int position){
        return articles.get(position);
    }

    public void removeArticle(int position){
        articles.remove(position);
        notifyItemRemoved(position);
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.recycler_item,parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Article article = articles.get(position);
        holder.tvId.setText(String.valueOf(article.getId()));
        holder.tvTitle.setText(article.getTitle());
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }
    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView tvId, tvTitle;

        public MyViewHolder(final View itemView) {
            super(itemView);
            tvId = itemView.findViewById(R.id.tvID);
            tvTitle = itemView.findViewById(R.id.tvTitle);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.itemClicked(getAdapterPosition());
                }
            });

            tvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.titleClick(getAdapterPosition());
                }
            });

        }
    }

}
