package com.example.rany.recyclerviewdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.rany.recyclerviewdemo.model.Article;

public class ResultScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_screen);

        Article article = (Article) getIntent().getParcelableArrayListExtra("article");
        Toast.makeText(this, article.toString(), Toast.LENGTH_SHORT).show();
    }
}
