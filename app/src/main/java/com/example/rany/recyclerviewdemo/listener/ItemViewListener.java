package com.example.rany.recyclerviewdemo.listener;

public interface ItemViewListener {

    void itemClicked(int position);
    void titleClick(int position);

}
