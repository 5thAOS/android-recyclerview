package com.example.rany.recyclerviewdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.rany.recyclerviewdemo.adapter.MyAdapter;
import com.example.rany.recyclerviewdemo.listener.ItemViewListener;
import com.example.rany.recyclerviewdemo.model.Article;

import java.util.ArrayList;

public class RecyclerDemo extends AppCompatActivity implements ItemViewListener{

    RecyclerView rcArticle;
    MyAdapter myAdapter;
    ArrayList<Article> articles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rcArticle = findViewById(R.id.rcArticle);
        articles = new ArrayList<>();
        for(int i = 1; i<= 50; i++){
            articles.add(new Article(i, "Article "+ i));
        }

        myAdapter = new MyAdapter();
        rcArticle.setLayoutManager(new GridLayoutManager(this, 2));
        rcArticle.setAdapter(myAdapter);

        myAdapter.addArticles(articles);
        myAdapter.setItemClickListener(this);
    }

    @Override
    public void itemClicked(int position) {
        Article article = (Article) myAdapter.getArticleItem(position);
        String id = String.valueOf(article.getId());
        String title = article.getTitle();

        Toast.makeText(this, id +" "+ title, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, ResultScreen.class);
        intent.putParcelableArrayListExtra("article", article);
        startActivity(intent);
    }

    @Override
    public void titleClick(int position) {
        myAdapter.removeArticle(position);
    }
}
